var e = require("../../@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
    value: !0
}), exports.default = void 0;

var r = e(require("../../@babel/runtime/helpers/defineProperty"));

function t(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
        var o = Object.getOwnPropertySymbols(e);
        r && (o = o.filter(function(r) {
            return Object.getOwnPropertyDescriptor(e, r).enumerable;
        })), t.push.apply(t, o);
    }
    return t;
}

var o = {
    selector: "#poster"
};

function n() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, n = arguments.length > 1 ? arguments[1] : void 0;
    e = function(e) {
        for (var o = 1; o < arguments.length; o++) {
            var n = null != arguments[o] ? arguments[o] : {};
            o % 2 ? t(Object(n), !0).forEach(function(t) {
                (0, r.default)(e, t, n[t]);
            }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : t(Object(n)).forEach(function(r) {
                Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(n, r));
            });
        }
        return e;
    }({}, o, {}, e);
    var c = getCurrentPages(), i = c[c.length - 1];
    n && (i = n);
    var l = i.selectComponent(e.selector);
    return delete e.selector, l;
}

n.create = function() {
    var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0], r = arguments.length > 1 ? arguments[1] : void 0;
    if (n({}, r)) return n({}, r).onCreate(e);
    console.error('请设置组件的id="poster"!!!');
};

var c = n;

exports.default = c;