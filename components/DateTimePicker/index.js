Component({
    properties: {
        timevalue: {
            type: Array,
            value: "标题"
        },
        years: {
            type: Array,
            value: "年"
        },
        months: {
            type: Array,
            value: "月"
        },
        days: {
            type: Array,
            value: "日"
        },
        hours: {
            type: Array,
            value: "小时"
        },
        minutes: {
            type: Array,
            value: "分钟"
        }
    },
    data: {},
    methods: {
        cancelbtn: function() {
            this.triggerEvent("cancelbtn");
        },
        closebtn: function() {
            this.triggerEvent("closebtn");
        },
        fnbindChange: function(e) {
            this.triggerEvent("bindChangeEvent", e.detail);
        }
    }
});