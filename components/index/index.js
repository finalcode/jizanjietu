var t = require("../../@babel/runtime/helpers/interopRequireDefault")(require("../../@babel/runtime/helpers/defineProperty"));

function e(t, e) {
    var i = Object.keys(t);
    if (Object.getOwnPropertySymbols) {
        var o = Object.getOwnPropertySymbols(t);
        e && (o = o.filter(function(e) {
            return Object.getOwnPropertyDescriptor(t, e).enumerable;
        })), i.push.apply(i, o);
    }
    return i;
}

function i(i) {
    for (var o = 1; o < arguments.length; o++) {
        var s = null != arguments[o] ? arguments[o] : {};
        o % 2 ? e(Object(s), !0).forEach(function(e) {
            (0, t.default)(i, e, s[e]);
        }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(i, Object.getOwnPropertyDescriptors(s)) : e(Object(s)).forEach(function(t) {
            Object.defineProperty(i, t, Object.getOwnPropertyDescriptor(s, t));
        });
    }
    return i;
}

var o = {
    drawBlock: function(t) {
        var e = t.text, i = t.width, o = void 0 === i ? 0 : i, s = t.height, r = t.x, n = t.y, h = t.paddingLeft, a = void 0 === h ? 0 : h, c = t.paddingRight, x = void 0 === c ? 0 : c, d = t.borderWidth, l = t.backgroundColor, u = t.borderColor, f = t.borderRadius, g = void 0 === f ? 0 : f, P = t.opacity, p = void 0 === P ? 1 : P, v = 0, b = 0, m = 0;
        if (void 0 !== e) {
            var w = this._getTextWidth("string" == typeof e.text ? e : e.text);
            v = w > o ? w : o, v += a + a;
            var y = e.textAlign, I = void 0 === y ? "left" : y;
            e.text;
            m = s / 2 + n, b = "left" === I ? r + a : "center" === I ? v / 2 + r : r + v - x;
        } else v = o;
        l && (this.ctx.save(), this.ctx.setGlobalAlpha(p), this.ctx.setFillStyle(l), g > 0 ? (this._drawRadiusRect(r, n, v, s, g), 
        this.ctx.fill()) : this.ctx.fillRect(this.toPx(r), this.toPx(n), this.toPx(v), this.toPx(s)), 
        this.ctx.restore()), d && (this.ctx.save(), this.ctx.setGlobalAlpha(p), this.ctx.setStrokeStyle(u), 
        this.ctx.setLineWidth(this.toPx(d)), g > 0 ? (this._drawRadiusRect(r, n, v, s, g), 
        this.ctx.stroke()) : this.ctx.strokeRect(this.toPx(r), this.toPx(n), this.toPx(v), this.toPx(s)), 
        this.ctx.restore()), e && this.drawText(Object.assign(e, {
            x: b,
            y: m
        }));
    },
    drawText: function(t) {
        var e = this, o = t.x, s = t.y, r = (t.fontSize, t.color, t.baseLine), n = (t.textAlign, 
        t.text);
        t.opacity, t.width, t.lineNum, t.lineHeight;
        if ("[object Array]" === Object.prototype.toString.call(n)) {
            var h = {
                x: o,
                y: s,
                baseLine: r
            };
            n.forEach(function(t) {
                h.x += t.marginLeft || 0;
                var o = e._drawSingleText(Object.assign(t, i({}, h)));
                h.x += o + (t.marginRight || 0);
            });
        } else this._drawSingleText(t);
    },
    drawImage: function(t) {
        var e = t.imgPath, i = t.x, o = t.y, s = t.w, r = t.h, n = t.sx, h = t.sy, a = t.sw, c = t.sh, x = t.borderRadius, d = void 0 === x ? 0 : x, l = t.borderWidth, u = void 0 === l ? 0 : l, f = t.borderColor;
        this.ctx.save(), d > 0 ? (this._drawRadiusRect(i, o, s, r, d), this.ctx.strokeStyle = "rgba(255,255,255,0)", 
        this.ctx.stroke(), this.ctx.clip(), this.ctx.drawImage(e, this.toPx(n), this.toPx(h), this.toPx(a), this.toPx(c), this.toPx(i), this.toPx(o), this.toPx(s), this.toPx(r)), 
        u > 0 && (this.ctx.setStrokeStyle(f), this.ctx.setLineWidth(this.toPx(u)), this.ctx.stroke())) : this.ctx.drawImage(e, this.toPx(n), this.toPx(h), this.toPx(a), this.toPx(c), this.toPx(i), this.toPx(o), this.toPx(s), this.toPx(r)), 
        this.ctx.restore();
    },
    drawLine: function(t) {
        var e = t.startX, i = t.startY, o = t.endX, s = t.endY, r = t.color, n = t.width;
        this.ctx.save(), this.ctx.beginPath(), this.ctx.setStrokeStyle(r), this.ctx.setLineWidth(this.toPx(n)), 
        this.ctx.moveTo(this.toPx(e), this.toPx(i)), this.ctx.lineTo(this.toPx(o), this.toPx(s)), 
        this.ctx.stroke(), this.ctx.closePath(), this.ctx.restore();
    },
    downloadResource: function(t) {
        var e = this, i = t.images, o = void 0 === i ? [] : i, s = t.pixelRatio, r = void 0 === s ? 1 : s, n = [];
        return this.drawArr = [], o.forEach(function(t, i) {
            return n.push(e._downloadImageAndInfo(t, i, r));
        }), Promise.all(n);
    },
    initCanvas: function(t, e, i) {
        var o = this;
        return new Promise(function(s) {
            o.setData({
                pxWidth: o.toPx(t),
                pxHeight: o.toPx(e),
                debug: i
            }, s);
        });
    }
}, s = {
    _drawRadiusRect: function(t, e, i, o, s) {
        var r = s / 2;
        this.ctx.beginPath(), this.ctx.moveTo(this.toPx(t + r), this.toPx(e)), this.ctx.lineTo(this.toPx(t + i - r), this.toPx(e)), 
        this.ctx.arc(this.toPx(t + i - r), this.toPx(e + r), this.toPx(r), 2 * Math.PI * .75, 2 * Math.PI * 1), 
        this.ctx.lineTo(this.toPx(t + i), this.toPx(e + o - r)), this.ctx.arc(this.toPx(t + i - r), this.toPx(e + o - r), this.toPx(r), 0, 2 * Math.PI * .25), 
        this.ctx.lineTo(this.toPx(t + r), this.toPx(e + o)), this.ctx.arc(this.toPx(t + r), this.toPx(e + o - r), this.toPx(r), 2 * Math.PI * .25, 2 * Math.PI * .5), 
        this.ctx.lineTo(this.toPx(t), this.toPx(e + r)), this.ctx.arc(this.toPx(t + r), this.toPx(e + r), this.toPx(r), 2 * Math.PI * .5, 2 * Math.PI * .75);
    },
    _getTextWidth: function(t) {
        var e = this, i = [];
        "[object Object]" === Object.prototype.toString.call(t) ? i.push(t) : i = t;
        var o = 0;
        return i.forEach(function(t) {
            var i = t.fontSize, s = t.text, r = t.marginLeft, n = void 0 === r ? 0 : r, h = t.marginRight, a = void 0 === h ? 0 : h;
            e.ctx.setFontSize(e.toPx(i)), o += e.ctx.measureText(s).width + n + a;
        }), this.toRpx(o);
    },
    _drawSingleText: function(t) {
        var e = this, i = t.x, o = t.y, s = t.fontSize, r = t.color, n = t.baseLine, h = t.textAlign, a = void 0 === h ? "left" : h, c = t.text, x = t.opacity, d = void 0 === x ? 1 : x, l = t.textDecoration, u = void 0 === l ? "none" : l, f = t.width, g = t.lineNum, P = void 0 === g ? 1 : g, p = t.lineHeight, v = void 0 === p ? 0 : p, b = t.fontWeight, m = void 0 === b ? "normal" : b, w = t.fontStyle, y = void 0 === w ? "normal" : w, I = t.fontFamily, R = void 0 === I ? "sans-serif" : I;
        this.ctx.save(), this.ctx.beginPath(), this.ctx.font = y + " " + m + " " + this.toPx(s, !0) + "px " + R, 
        this.ctx.setGlobalAlpha(d), this.ctx.setFillStyle(r), this.ctx.setTextBaseline(n), 
        this.ctx.setTextAlign(a);
        var S = this.toRpx(this.ctx.measureText(c).width), T = [];
        if (S > f) {
            for (var O = "", k = 1, j = 0; j <= c.length - 1; j++) O += c[j], this.toRpx(this.ctx.measureText(O).width) >= f ? (k === P && j !== c.length - 1 && (O = O.substring(0, O.length - 1) + "..."), 
            k <= P && T.push(O), O = "", k++) : k <= P && j === c.length - 1 && T.push(O);
            S = f;
        } else T.push(c);
        if (T.forEach(function(t, r) {
            e.ctx.fillText(t, e.toPx(i), e.toPx(o + (v || s) * r));
        }), this.ctx.restore(), "none" !== u) {
            var _ = o;
            if ("line-through" === u) {
                _ = o;
                switch (n) {
                  case "top":
                    _ += s / 2 + 5;
                    break;

                  case "middle":
                    break;

                  case "bottom":
                    _ -= s / 2 + 5;
                    break;

                  default:
                    _ -= s / 2 - 5;
                }
            }
            this.ctx.save(), this.ctx.moveTo(this.toPx(i), this.toPx(_)), this.ctx.lineTo(this.toPx(i) + this.toPx(S), this.toPx(_)), 
            this.ctx.setStrokeStyle(r), this.ctx.stroke(), this.ctx.restore();
        }
        return S;
    }
}, r = {
    _downloadImageAndInfo: function(t, e, i) {
        var o = this;
        return new Promise(function(s, r) {
            var n = t.x, h = t.y, a = t.url, c = t.zIndex, x = a;
            o._downImage(x, e).then(function(t) {
                return o._getImageInfo(t, e);
            }).then(function(r) {
                var a, x, d = r.imgPath, l = r.imgInfo, u = t.borderRadius || 0, f = t.width, g = t.height, P = o.toRpx(l.width / i), p = o.toRpx(l.height / i);
                P / p <= f / g ? (a = 0, x = (p - P / f * g) / 2) : (x = 0, a = (P - p / g * f) / 2), 
                o.drawArr.push({
                    type: "image",
                    borderRadius: u,
                    borderWidth: t.borderWidth,
                    borderColor: t.borderColor,
                    zIndex: void 0 !== c ? c : e,
                    imgPath: d,
                    sx: a,
                    sy: x,
                    sw: P - 2 * a,
                    sh: p - 2 * x,
                    x: n,
                    y: h,
                    w: f,
                    h: g
                }), s();
            }).catch(function(t) {
                return r(t);
            });
        });
    },
    _downImage: function(t) {
        var e = this;
        return new Promise(function(i, o) {
            /^http/.test(t) && !new RegExp(wx.env.USER_DATA_PATH).test(t) ? wx.downloadFile({
                url: e._mapHttpToHttps(t),
                success: function(t) {
                    200 === t.statusCode ? i(t.tempFilePath) : o(t.errMsg);
                },
                fail: function(t) {
                    o(t);
                }
            }) : i(t);
        });
    },
    _getImageInfo: function(t, e) {
        return new Promise(function(i, o) {
            wx.getImageInfo({
                src: t,
                success: function(o) {
                    i({
                        imgPath: t,
                        imgInfo: o,
                        index: e
                    });
                },
                fail: function(t) {
                    o(t);
                }
            });
        });
    },
    toPx: function(t, e) {
        return e ? parseInt(t * this.factor * this.pixelRatio) : t * this.factor * this.pixelRatio;
    },
    toRpx: function(t, e) {
        return e ? parseInt(t / this.factor) : t / this.factor;
    },
    _mapHttpToHttps: function(t) {
        if (t.indexOf(":") < 0) return t;
        var e = t.split(":");
        return 2 === e.length && "http" === e[0] ? (e[0] = "https", "".concat(e[0], ":").concat(e[1])) : t;
    }
};

Component({
    properties: {},
    created: function() {
        var t = wx.getSystemInfoSync().screenWidth;
        this.factor = t / 750;
    },
    methods: Object.assign({
        getHeight: function(t) {
            var e = function(t) {
                var e = t.lineHeight || t.fontSize;
                return "top" === t.baseLine ? e : "middle" === t.baseLine ? e / 2 : 0;
            }, o = [];
            (t.blocks || []).forEach(function(t) {
                o.push(t.y + t.height);
            }), (t.texts || []).forEach(function(t) {
                var s;
                "[object Array]" === Object.prototype.toString.call(t.text) ? t.text.forEach(function(r) {
                    s = e(i({}, r, {
                        baseLine: t.baseLine
                    })), o.push(t.y + s);
                }) : (s = e(t), o.push(t.y + s));
            }), (t.images || []).forEach(function(t) {
                o.push(t.y + t.height);
            }), (t.lines || []).forEach(function(t) {
                o.push(t.startY), o.push(t.endY);
            });
            var s = o.sort(function(t, e) {
                return e - t;
            }), r = 0;
            return s.length > 0 && (r = s[0]), t.height < r || !t.height ? r : t.height;
        },
        create: function(t) {
            var e = this;
            this.ctx = wx.createCanvasContext("canvasid", this), this.pixelRatio = t.pixelRatio || 1;
            var i = this.getHeight(t);
            this.initCanvas(t.width, i, t.debug).then(function() {
                t.backgroundColor && (e.ctx.save(), e.ctx.setFillStyle(t.backgroundColor), e.ctx.fillRect(0, 0, e.toPx(t.width), e.toPx(i)), 
                e.ctx.restore());
                var o = t.texts, s = void 0 === o ? [] : o, r = (t.images, t.blocks), n = void 0 === r ? [] : r, h = t.lines, a = void 0 === h ? [] : h, c = e.drawArr.concat(s.map(function(t) {
                    return t.type = "text", t.zIndex = t.zIndex || 0, t;
                })).concat(n.map(function(t) {
                    return t.type = "block", t.zIndex = t.zIndex || 0, t;
                })).concat(a.map(function(t) {
                    return t.type = "line", t.zIndex = t.zIndex || 0, t;
                }));
                c.sort(function(t, e) {
                    return t.zIndex - e.zIndex;
                }), c.forEach(function(t) {
                    "image" === t.type ? e.drawImage(t) : "text" === t.type ? e.drawText(t) : "block" === t.type ? e.drawBlock(t) : "line" === t.type && e.drawLine(t);
                });
                var x = wx.getSystemInfoSync().platform, d = 0;
                "android" === x && (d = 300), e.ctx.draw(!1, function() {
                    setTimeout(function() {
                        wx.canvasToTempFilePath({
                            canvasId: "canvasid",
                            success: function(t) {
                                e.triggerEvent("success", t.tempFilePath);
                            },
                            fail: function(t) {
                                e.triggerEvent("fail", t);
                            }
                        }, e);
                    }, d);
                });
            }).catch(function(t) {
                wx.showToast({
                    icon: "none",
                    title: t.errMsg || "生成失败"
                }), console.error(t);
            });
        }
    }, o, s, r)
});