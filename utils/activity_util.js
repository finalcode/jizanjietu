module.exports = {
    getDate: function(t, e, s, r, n) {
        return t.substr(0, t.length - 1) + "年" + e.substr(0, e.length - 1) + "月" + s.substr(0, s.length - 1) + "日 " + (r < 10 ? "0" + r : r) + ":" + (n < 10 ? "0" + n : n);
    },
    getobjDate: function(t) {
        var e, s = (e = t ? new Date(t) : new Date()).getFullYear(), r = e.getMonth() + 1, n = e.getDate(), i = e.getHours(), u = e.getMinutes();
        Math.ceil(e.getMinutes() / 10);
        return s + "年" + r + "月" + n + "日 " + (i < 10 ? "0" + i : i) + ":" + (u < 10 ? "0" + u : u);
    },
    mGetDate: function(t, e) {
        return new Date(t, e, 0).getDate();
    },
    getarrWithtime: function(t) {
        var e = [];
        e[0] = t.split("年")[0], e[1] = t.split("年")[1].split("月")[0], e[2] = t.split("年")[1].split("月")[1].split("日")[0];
        var s = t.split(" ")[1];
        return e[3] = s.split(":")[0], e[4] = s.split(":")[1], e[1] = e[1].startsWith("0") ? e[1].substr(1, e[1].length) : e[1], 
        e[2] = e[2].startsWith("0") ? e[2].substr(1, e[2].length) : e[2], e[3] = e[3].startsWith("0") ? e[3].substr(1, e[3].length) : e[3], 
        e[4] = e[4].startsWith("0") ? e[4].substr(1, e[4].length) : e[4], e;
    }
};