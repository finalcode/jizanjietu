App({
    onLaunch: function() {
        wx.cloud ? wx.cloud.init({
            env: "screen-gen-cloud-pdl5y",
            traceUser: !0
        }) : console.error("请使用 2.2.3 或以上的基础库以使用云能力"), this.globalData = {};
    },
    globalData: {
        userInfo: {},
        openid: "",
        userType: "0",
        previousPage: "start",
        hasWathced: !1,
        hasCreatedToday: !1,
        admode: -1,
        globalCreateTimes: 0,
        adShowTime: 3,
        uploadContent: 0
    }
});