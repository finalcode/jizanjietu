var t = getApp();

Page({
    data: {
        avatarUrl: "./user-unlogin.png",
        userInfo: {},
        takeSession: !1,
        requestResult: "",
        hasUserInfo: !1,
        clickable: !1,
        mode: "mode",
        updated: !1,
        defaultMsg: "载入中",
        isShowDialog: !1,
        dialogText: "生成截图需要您的头像和昵称\n点击'重新获取'再次尝试获取授权\n您也可以手动输入昵称和上传头像",
        b1text: "手动输入",
        b2text: "重新获取",
        step: 0,
        nickName: "",
        tempFilePaths: "",
        photook: !0,
        uploadtext: "点击此处上传头像"
    },
    onLoad: function() {
        var e = this;
        t.globalData.hasWathced = !1, t.globalData.globalCreateTimes = 0, wx.cloud ? (wx.getSetting({
            success: function(a) {
                a.authSetting["scope.userInfo"] && wx.getUserInfo({
                    success: function(a) {
                        t.globalData.userInfo = a.userInfo, e.setData({
                            avatarUrl: a.userInfo.avatarUrl,
                            userInfo: a.userInfo,
                            hasUserInfo: !0
                        });
                    }
                });
            }
        }), wx.cloud.callFunction({
            name: "login",
            data: {},
            success: function(e) {
                console.log("[云函数] [login] user openid: ", e.result.openid), t.globalData.openid = e.result.openid, 
                t.globalData.userType = e.result.userType, t.globalData.admode = e.result.admode, 
                t.globalData.adShowTime = e.result.adShowTime, t.globalData.uploadContent = e.result.uploadContent;
            },
            fail: function(t) {
                console.error("[云函数] [login] 调用失败", t), wx.showToast({
                    title: "网络似乎有些问题 稍后再试吧",
                    duration: 2e3,
                    icon: "none"
                });
            },
            complete: function(t) {
                e.setData({
                    clickable: !0,
                    defaultMsg: "一键导入头像和昵称 并下一步"
                });
            }
        })) : wx.showToast({
            title: "网络似乎有些问题 稍后再试吧",
            duration: 2e3,
            icon: "none"
        });
    },
    onGetUserInfo: function(e) {
        this.setData({
            isShowDialog: !1,
            b1text: "手动输入",
            b2text: "重新获取",
            step: 0
        }), !this.data.hasUserInfo && e.detail.userInfo ? (t.globalData.userInfo = e.detail.userInfo, 
        this.setData({
            hasUserInfo: !0,
            avatarUrl: e.detail.userInfo.avatarUrl,
            userInfo: e.detail.userInfo
        }), this.onAdd(), "mode" == this.data.mode ? wx.showToast({
            title: "请选择样式",
            icon: "none"
        }) : "mode1" == this.data.mode ? wx.navigateTo({
            url: "mode1"
        }) : "mode2" == this.data.mode && wx.navigateTo({
            url: "mode2"
        })) : this.setData({
            isShowDialog: !0
        });
    },
    tapDialogButton1: function(t) {
        0 == this.data.step ? this.setData({
            step: 1,
            b1text: "取消",
            b2text: "确认"
        }) : 1 == this.data.step && (wx.showToast({
            title: "已取消，请重试",
            icon: "none"
        }), this.setData({
            isShowDialog: !1,
            step: 0
        }));
    },
    tapDialogButton2: function(e) {
        var a = this;
        0 == this.data.step ? (this.setData({
            isShowDialog: !1
        }), this.onGetUserInfo()) : 1 == this.data.step && (this.data.nickName.length > 0 && this.data.tempFilePaths.length > 0 ? wx.cloud.callFunction({
            name: "msgcheck",
            data: {
                content: this.data.nickName
            },
            success: function(e) {
                0 == e.result.errCode ? (a.setData({
                    isShowDialog: !1,
                    step: 0,
                    hasUserInfo: !0
                }), t.globalData.userInfo = {
                    nickName: a.data.nickName,
                    avatarUrl: a.data.tempFilePaths
                }, a.onAdd(), "mode" == a.data.mode ? wx.showToast({
                    title: "请选择样式",
                    icon: "none"
                }) : "mode1" == a.data.mode ? wx.navigateTo({
                    url: "mode1"
                }) : "mode2" == a.data.mode && wx.navigateTo({
                    url: "mode2"
                })) : 87014 == e.result.errCode ? wx.showToast({
                    title: "昵称请勿输入与政治有关或涉及黄赌毒的词语",
                    duration: 1500,
                    mask: !0,
                    icon: "none"
                }) : wx.showToast({
                    title: "网络有点小问题，稍后再试吧",
                    duration: 1500,
                    mask: !0,
                    icon: "none"
                });
            }
        }) : wx.showToast({
            title: "昵称和头像不能为空",
            icon: "none"
        }));
    },
    nameInput: function(t) {
        this.setData({
            nickName: t.detail.value
        });
    },
    chooseImage: function(t) {
        var e = this;
        wx.chooseImage({
            count: 1,
            sizeType: [ "compressed" ],
            sourceType: [ "album" ],
            success: function(t) {
                e.setData({
                    tempFilePaths: t.tempFilePaths[0],
                    photook: !1,
                    uploadtext: "图片处理中",
                    b2text: "请稍候"
                }), e.checkImg(t.tempFilePaths[0]);
            }
        });
    },
    checkImg: function(e) {
        var a = this;
        wx.getImageInfo({
            src: e,
            success: function(e) {
                if ("4" == t.globalData.userType) {
                    var o = wx.createCanvasContext("firstCanvas"), s = e.height, n = e.width;
                    e.width / e.height < .56 ? e.height > 334 && (s = 334, n = 334 * e.width / e.height) : e.width > 188 && (n = 188, 
                    s = 188 * e.height / e.width), o.drawImage(e.path, 0, 0, n, s), o.draw(!1, function() {
                        wx.canvasToTempFilePath({
                            x: 0,
                            y: 0,
                            width: n,
                            height: s,
                            destWidth: n,
                            destHeight: s,
                            canvasId: "firstCanvas",
                            success: function(t) {
                                wx.getFileSystemManager().readFile({
                                    filePath: t.tempFilePath,
                                    encoding: "base64",
                                    success: function(t) {
                                        wx.cloud.callFunction({
                                            name: "imgcheck",
                                            data: {
                                                img: t.data
                                            },
                                            success: function(t) {
                                                console.log(t), 87014 == t.result.errCode ? (wx.showToast({
                                                    title: "图片含有违法违规内容，请选择其他图片",
                                                    icon: "none",
                                                    duration: 2e3
                                                }), a.setData({
                                                    tempFilePaths: "",
                                                    uploadtext: "失败 点击重新上传",
                                                    photook: !1,
                                                    b2text: "请上传头像"
                                                })) : a.setData({
                                                    photook: !0,
                                                    uploadtext: "已上传 点击更改",
                                                    b2text: "确认"
                                                });
                                            },
                                            fail: function(t) {
                                                console.log(t), a.setData({
                                                    photook: !0,
                                                    uploadtext: "已上传 点击重新上传",
                                                    b2text: "确认"
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    });
                } else a.setData({
                    photook: !0,
                    uploadtext: "已上传 点击重新上传",
                    b2text: "确认"
                });
            }
        });
    },
    onAdd: function() {
        var e = this;
        this.data.updated || wx.cloud.database().collection("users").add({
            data: {
                _id: t.globalData.openid,
                userInfo: this.data.userInfo
            },
            success: function(t) {
                console.log("[数据库] [新增记录] 成功，记录 _id: ", t._id);
            },
            fail: function(a) {
                var o = e;
                wx.cloud.database().collection("users").doc(t.globalData.openid).update({
                    data: {
                        userInfo: o.data.userInfo
                    },
                    success: function(t) {
                        console.log("[数据库] [修改记录] 成功，记录 _id: ");
                    },
                    fail: function(t) {
                        console.log("[数据库] [修改记录] 失败", t);
                    }
                });
            },
            complete: function(a) {
                wx.cloud.database().collection("users").doc(t.globalData.openid).get({
                    success: function(e) {
                        var a, o = new Date(), s = e.data.createdDate;
                        for (a = 0; a < s.length; a++) if (o.getDate() == s[a].getDate() && o.getMonth() == s[a].getMonth() && o.getFullYear() == s[a].getFullYear()) {
                            t.globalData.hasCreatedToday = !0;
                            break;
                        }
                        var n = e.data.watchVideo;
                        for (a = 0; a < n.length; a++) if (o.getDate() == n[a].getDate() && o.getMonth() == n[a].getMonth() && o.getFullYear() == n[a].getFullYear()) {
                            t.globalData.hasWathced = !0;
                            break;
                        }
                    },
                    fail: function(t) {
                        console.log("[数据库] [新增记录] 失败", t);
                    }
                }), e.setData({
                    updated: !0
                });
            }
        });
    },
    mode1: function(t) {
        this.onAdd();
        "mode" == this.data.mode ? wx.showToast({
            title: "请选择样式",
            icon: "none"
        }) : "mode1" == this.data.mode ? wx.navigateTo({
            url: "mode1"
        }) : "mode2" == this.data.mode && wx.navigateTo({
            url: "mode2"
        });
    },
    handleChange: function(t) {
        this.setData({
            mode: t.detail.value
        });
    },
    onShareAppMessage: function(t) {
        return t.from, {
            title: "我刚刚用了朋友圈集赞截图生成器，从此集赞不求人，你也试试",
            path: "/pages/index/index",
            imageUrl: "/images/share.jpg",
            success: function(t) {}
        };
    },
    onHide: function() {
        t.globalData.previousPage = "index";
    }
});