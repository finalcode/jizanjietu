var t = require("../../@babel/runtime/helpers/interopRequireDefault"), e = t(require("../../@babel/runtime/helpers/toConsumableArray"));

require("../../@babel/runtime/helpers/Arrayincludes");

for (var a = t(require("../../components/poster/poster")), i = require("../../utils/activity_util"), o = getApp(), n = null, s = [ 22, 15, 20, 22, 23, 21, 22, 21, 22, 22, 11 ], r = {
    width: 828,
    height: 1792,
    backgroundColor: "#fff",
    debug: !1,
    pixelRatio: 1,
    blocks: [ {
        width: 780,
        height: 88,
        x: 20,
        y: 580,
        borderWidth: 0,
        backgroundColor: "#f7f7f7",
        zIndex: 6
    } ],
    texts: [ {
        x: 125,
        y: 217,
        baseLine: "middle",
        lineNum: 1,
        text: "点击右侧头像以加载头像",
        fontWeight: "bold",
        fontSize: 36,
        color: "#5b6291",
        zIndex: 200
    }, {
        x: 125,
        y: 267,
        fontSize: 34,
        baseLine: "middle",
        text: "点击这里输入一行内容",
        width: 620,
        lineNum: 20,
        lineHeight: 45,
        color: "#000000",
        zIndex: 200
    }, {
        x: 125,
        y: 430,
        fontSize: 28,
        baseLine: "middle",
        text: "2019年1月1日 12:34",
        width: 570,
        lineNum: 1,
        color: "#737373",
        zIndex: 200
    }, {
        x: 430,
        y: 430,
        fontSize: 28,
        baseLine: "middle",
        text: "删除",
        width: 570,
        lineNum: 1,
        color: "#5B6A91",
        zIndex: 200
    } ],
    images: [ {
        width: 80,
        height: 80,
        x: 25,
        y: 200,
        borderRadius: 15,
        url: "../../images/icon/0.jpg",
        zIndex: 30
    }, {
        width: 828,
        height: 182,
        x: 0,
        y: 0,
        url: "../../images/top.png",
        zIndex: 1
    }, {
        width: 828,
        height: 188,
        x: 0,
        y: 125,
        url: "../../images/back1.png",
        zIndex: 5
    }, {
        width: 828,
        height: 176,
        x: 0,
        y: 1616,
        url: "../../images/down.png",
        zIndex: 300
    }, {
        width: 22,
        height: 30,
        x: 58,
        y: 36,
        url: "../../images/time/0.png",
        zIndex: 50
    }, {
        width: 22,
        height: 30,
        x: 78,
        y: 36,
        url: "../../images/time/0.png",
        zIndex: 50
    }, {
        width: 11,
        height: 30,
        x: 98,
        y: 36,
        url: "../../images/time/c.png",
        zIndex: 50
    }, {
        width: 22,
        height: 30,
        x: 106,
        y: 36,
        url: "../../images/time/0.png",
        zIndex: 50
    }, {
        width: 22,
        height: 30,
        x: 126,
        y: 36,
        url: "../../images/time/0.png",
        zIndex: 50
    } ]
}, l = (new Date(), []), h = [], d = [], g = [], u = [], c = 2e3; c <= 2050; c++) l.push(c + "年");

for (var m = 1; m <= 12; m++) h.push(m + "月");

for (var f = 1; f <= 31; f++) d.push(f + "日");

for (var p = 0; p <= 23; p++) g.push(p + "");

for (var x = 0; x <= 59; x++) u.push(x + "");

Page({
    data: {
        userInfo: o.globalData.userInfo,
        hasUserInfo: !0,
        nextable: !0,
        date: "修改日期",
        time: "修改时间",
        number: 1,
        content: " ",
        tempFilePaths: [],
        samplePhoto: "../../images/icon/777.jpg",
        openid: "",
        firstTime: !0,
        isSubtitle: !1,
        pictureNum: 0,
        text1: "选择",
        text2: "图片",
        onePhoto: !1,
        defaultMsg: "生成",
        createTimes: 0,
        isShowDialog: !1,
        dialogText: "今日生成次数已用完，观看广告后即可无限次生成",
        dialogN: 0,
        timeChanged: !1,
        openflag: !0,
        years: l,
        months: h,
        days: d,
        hours: g,
        minutes: u,
        year: "",
        month: "",
        day: "",
        hour: "",
        minute: "",
        value: [ 0, 1, 1, 1, 1 ],
        starttime: i.getobjDate(),
        systime: "",
        tips: "若图无法生成，请点击右上·•·重新进入小程序",
        cloudPaths: []
    },
    onLoad: function() {
        var t = this, e = new Date();
        if (e.getHours() < 10) var a = "0" + e.getHours().toString(); else a = e.getHours().toString();
        if (e.getMinutes() < 10) var i = "0" + e.getMinutes().toString(); else i = e.getMinutes().toString();
        this.setData({
            userInfo: o.globalData.userInfo,
            openid: o.globalData.openid,
            systime: a + ":" + i
        }), wx.createRewardedVideoAd && ((n = wx.createRewardedVideoAd({
            adUnitId: "adunit-bec0983627de10de"
        })).onLoad(function() {}), n.onError(function(t) {
            wx.reportAnalytics("rewardedvideoerr", {
                code: t.errCode
            }), o.globalData.hasWathced = !0;
        }), n.onClose(function(e) {
            t.watchSuccess();
        }));
    },
    watchSuccess: function() {
        this.createPoster(), o.globalData.hasWathced = !0;
        var t = wx.cloud.database(), e = t.command;
        t.collection("users").doc(o.globalData.openid).update({
            data: {
                watchVideo: e.push(new Date())
            },
            success: function(t) {
                console.log("[数据库] [新增记录] 成功", t);
            },
            fail: function(t) {
                console.log("[数据库] [新增记录] 失败", t);
            }
        });
    },
    onShow: function() {},
    tap: function(t) {
        var e, a = [ 2019, 0, 0, 0, 0 ];
        e = i.getarrWithtime(this.data.starttime);
        var o = this.data, n = o.years, s = o.months, r = o.days, l = o.hours, h = o.minutes;
        o.openflag;
        a[0] = n.indexOf(e[0] + "年"), a[1] = s.indexOf(e[1] + "月"), a[2] = r.indexOf(e[2] + "日"), 
        a[3] = l.indexOf(e[3]), a[4] = h.indexOf(e[4]), this.setData({
            value: a,
            openflag: !1,
            years: n,
            months: s,
            days: r,
            hours: l,
            minutes: h
        });
    },
    cancelbtn: function() {
        this.setData({
            openflag: !0,
            changefalg: !1
        });
    },
    closebtn: function() {
        this.setData({
            openflag: !0
        });
        var t = this.data, e = (t.curindex, t.year), a = t.month, o = t.day, n = t.hour, s = t.minute;
        if (this.data.changefalg) {
            var r = i.getDate(e, a, o, n, s);
            this.setData({
                starttime: r,
                changefalg: !1
            });
        }
    },
    bindChange: function(t) {
        for (var e = t.detail.value, a = this.data.years[e[0]], o = this.data.months[e[1]], n = this.data.days[e[2]], s = this.data.hours[e[3]], r = this.data.minutes[e[4]], l = [], h = i.mGetDate(a.substr(0, a.length - 1), o.substr(0, o.length - 1)), d = 1; d <= h; d++) l.push(d + "日");
        this.setData({
            days: l,
            year: a,
            month: o,
            day: n,
            hour: s,
            minute: r,
            changefalg: !0
        });
    },
    onPosterSuccess: function(t) {
        var e = t.detail;
        wx.navigateTo({
            url: "preview?path=" + e
        });
    },
    onPosterFail: function(t) {
        console.error(t), wx.showToast({
            title: "网络似乎有点小问题，稍后再试吧",
            duration: 1500,
            icon: "none"
        });
    },
    length: function(t) {
        var e = 0, a = 0;
        for (a = 0; a < t.length; a++) t.charCodeAt(a) < 64 || t.charCodeAt(a) > 90 && t.charCodeAt(a) < 127 ? e += 1 : t.charCodeAt(a) >= 64 && t.charCodeAt(a) <= 90 ? e += 1.25 : e += 1.83;
        return Math.floor(e / 39) + 1;
    },
    tapDialogButton1: function(t) {
        1 == this.data.dialogN ? this.setData({
            dialogN: 2,
            dialogText: "我们需要您的帮助以支付服务器成本，广告只有几秒"
        }) : (this.setData({
            isShowDialog: !1
        }), wx.reportAnalytics("cancalad", {
            createtimes: this.data.createTimes
        }), wx.showToast({
            title: "生成失败，请重试",
            duration: 2e3,
            mask: !0,
            icon: "none"
        }));
    },
    tapDialogButton2: function(t) {
        var e = this;
        this.setData({
            isShowDialog: !1
        }), n && n.show().catch(function() {
            n.load().then(function() {
                return n.show();
            }).catch(function(t) {
                e.createPoster(), console.log("激励视频 广告显示失败"), o.globalData.hasWathced = !0;
            });
        });
    },
    onCreatePoster: function() {
        this.setData({
            userInfo: o.globalData.userInfo,
            openid: o.globalData.openid
        }), 0 == this.data.tempFilePaths.length ? wx.showToast({
            title: "请选择图片",
            duration: 1500,
            mask: !0,
            icon: "none"
        }) : -1 != o.globalData.admode && o.globalData.globalCreateTimes > o.globalData.adShowTime && o.globalData.hasCreatedToday && !o.globalData.hasWathced ? this.setData({
            isShowDialog: !0,
            dialogN: 1,
            dialogText: "今日生成次数已用完，观看广告后即可无限次生成"
        }) : (wx.reportAnalytics("create", {
            create: 1
        }), this.createPoster());
    },
    createPoster: function() {
        var t = this;
        this.data.content.length > 200 && this.setData({
            tips: "若图无法生成，请点击右上·•·重新进入小程序。\n若图文重合，请在文字最后手动加多个空格。"
        });
        var i = this.data.createTimes + 1;
        if (!o.globalData.hasCreatedToday) {
            o.globalData.hasCreatedToday = !0;
            var n = wx.cloud.database(), l = n.command;
            n.collection("users").doc(o.globalData.openid).update({
                data: {
                    createdDate: l.push(new Date())
                },
                success: function(t) {},
                fail: function(t) {}
            });
        }
        o.globalData.globalCreateTimes = o.globalData.globalCreateTimes + 1, this.setData({
            createTimes: i
        });
        var h, d, g = this.data.tempFilePaths, u = 0, c = this.data.content.replace(/\n/g, " "), m = JSON.parse(JSON.stringify(r)), f = this.length(c), p = parseInt(this.data.systime[0]), x = parseInt(this.data.systime[1]), w = parseInt(this.data.systime[3]), b = parseInt(this.data.systime[4]), D = 58;
        m.images[4].x = D, m.images[4].width = s[p], m.images[4].url = "../../images/time/" + p.toString() + ".png", 
        D += s[p], m.images[5].x = D, m.images[5].width = s[x], m.images[5].url = "../../images/time/" + x.toString() + ".png", 
        D += s[x], m.images[6].x = D, D += 11, m.images[7].x = D, m.images[7].width = s[w], 
        m.images[7].url = "../../images/time/" + w.toString() + ".png", D += s[w], m.images[8].x = D, 
        m.images[8].width = s[b], m.images[8].url = "../../images/time/" + b.toString() + ".png", 
        m.texts[0].text = this.data.userInfo.nickName, m.texts[1].text = c, this.setData({
            onePhoto: 1 == g.length
        });
        var y = 1;
        if (1 == g.length) m.images.push({
            width: 350,
            height: 350,
            x: 124,
            y: 250 + 45 * f,
            mode: "aspectFill",
            borderRadius: 0,
            url: g[0],
            zIndex: 30
        }), u = 250 + 45 * f + 360; else {
            for (h = 0; h < Math.floor((g.length + 2) / 3); h++) for (d = 0; d < 3 && (m.images.push({
                width: 170,
                height: 170,
                x: 124 + 180 * d,
                y: 250 + 45 * f + 180 * h,
                borderRadius: 0,
                url: g[3 * h + d],
                zIndex: 30
            }), y != g.length); d++) y += 1;
            u = 250 + 45 * f + 180 * Math.floor((g.length + 2) / 3);
        }
        m.images[0].url = this.data.userInfo.avatarUrl, m.images[2].y = u;
        var v = this.data.starttime, S = new Date(), I = S.getFullYear().toString() + "年" + (S.getMonth() + 1).toString() + "月" + S.getDate().toString() + "日 ";
        v.includes(I) && (v = v.replace(I, ""), m.texts[3].x = 240), I = S.getFullYear().toString() + "年" + (S.getMonth() + 1).toString() + "月" + (S.getDate() - 1).toString() + "日 ", 
        v.includes(I) && (v = v.replace(I, "昨天 "), m.texts[3].x = 300), m.texts[2].text = v, 
        m.texts[2].y = u + 33, m.texts[3].y = u + 33, y = 1;
        for (var P, C, T = (0, e.default)(Array(217).keys()), z = T.length; 0 !== z; ) C = Math.floor(Math.random() * z), 
        P = T[z -= 1], T[z] = T[C], T[C] = P;
        var F = this.data.number;
        for (1 == F && (F = 2), h = 0; h < Math.floor(this.data.number / 8); h++) for (d = 0; d < 8; d++) m.images.push({
            width: 70,
            height: 70,
            x: 90 + 88 * d,
            y: 95 + u + 88 * h,
            borderRadius: 15,
            url: "../../images/icon/" + T.pop().toString() + ".jpg",
            zIndex: 30
        }), y += 1;
        var M = 95 + u + 88 * Math.floor(this.data.number / 8);
        for (h = 0; h < this.data.number % 8; h++) m.images.push({
            width: 70,
            height: 70,
            x: 90 + 88 * h,
            y: M,
            borderRadius: 15,
            url: "../../images/icon/" + T.pop().toString() + ".jpg",
            zIndex: 30
        }), y += 1;
        m.blocks[0].height = 88 * Math.floor((F - 1) / 8) + 18, m.blocks[0].y = 160 + u, 
        M + 88 > 1750 && (m.images[3].y = M + 70), wx.cloud.callFunction({
            name: "msgcheck",
            data: {
                content: this.data.content
            },
            success: function(e) {
                if (0 == e.result.errCode) {
                    if (console.log(t.data.cloudPaths), 1 == o.globalData.uploadContent) wx.cloud.database().collection("content1").add({
                        data: {
                            mode: 2,
                            content: t.data.content,
                            number: t.data.number,
                            time: new Date(),
                            cloudPaths: t.data.cloudPaths
                        },
                        complete: function(e) {
                            t.setData({
                                cloudPaths: []
                            });
                        }
                    });
                    t.setData({
                        posterConfig: m
                    }, function() {
                        a.default.create(!0);
                    });
                } else 87014 == e.result.errCode ? wx.showToast({
                    title: "请勿输入与政治有关或涉及黄赌毒的词语",
                    duration: 1500,
                    mask: !0,
                    icon: "none"
                }) : wx.showToast({
                    title: "服务器有点小问题，稍后再试吧",
                    duration: 1500,
                    mask: !0,
                    icon: "none"
                });
            }
        });
    },
    sliderChange: function(t) {
        var e = t.detail.value;
        this.setData({
            number: e
        });
    },
    contentInput: function(t) {
        this.setData({
            content: t.detail.value
        });
    },
    chooseImage: function(t) {
        var e = this;
        wx.chooseImage({
            count: 9,
            sizeType: [ "compressed" ],
            sourceType: [ "album" ],
            success: function(t) {
                e.setData({
                    tempFilePaths: t.tempFilePaths,
                    text1: "您已选择" + t.tempFilePaths.length.toString(10) + "张图片",
                    text2: "正在处理",
                    nextable: !1,
                    defaultMsg: "图片处理中"
                }), t.tempFilePaths.length > 1 && e.setData({
                    onePhoto: !1
                }), e.data.r = 0;
                e.uploadOneByOne(t.tempFilePaths, 0, t.tempFilePaths.length);
            }
        });
    },
    uploadOneByOne: function(t, e, a) {
        var i = this, n = t[e];
        wx.getImageInfo({
            src: n,
            success: function(n) {
                var s = new Date();
                if (wx.cloud.uploadFile({
                    cloudPath: s.getFullYear().toString() + s.getMonth().toString() + s.getDate().toString() + "/" + o.globalData.openid + s.getTime().toString(10) + ".jpg",
                    filePath: n.path,
                    success: function(t) {
                        i.data.cloudPaths.push(t.fileID), console.log(i.data.cloudPaths);
                    }
                }), "4" == o.globalData.userType) {
                    var r = wx.createCanvasContext("firstCanvas"), l = n.height, h = n.width;
                    n.width / n.height < .56 ? n.height > 334 && (l = 334, h = 334 * n.width / n.height) : n.width > 188 && (h = 188, 
                    l = 188 * n.height / n.width), r.drawImage(n.path, 0, 0, h, l), r.draw(!1, function() {
                        wx.canvasToTempFilePath({
                            x: 0,
                            y: 0,
                            width: h,
                            height: l,
                            destWidth: h,
                            destHeight: l,
                            canvasId: "firstCanvas",
                            success: function(o) {
                                wx.getFileSystemManager().readFile({
                                    filePath: o.tempFilePath,
                                    encoding: "base64",
                                    success: function(t) {},
                                    fail: function(t) {},
                                    complete: function(o) {
                                        e++, wx.cloud.callFunction({
                                            name: "imgcheck",
                                            data: {
                                                img: o.data
                                            },
                                            success: function(o) {
                                                87014 == o.result.errCode ? (wx.showToast({
                                                    title: "您上传的第" + e.toString(10) + "张图片含有违法违规内容，请重新依次选择全部图片",
                                                    icon: "none",
                                                    duration: 2e3
                                                }), i.setData({
                                                    text1: "请重新",
                                                    text2: "依次选择图片",
                                                    tempFilePaths: [],
                                                    defaultMsg: "请重新选择图片"
                                                })) : e == a ? i.setData({
                                                    nextable: !0,
                                                    defaultMsg: "生成",
                                                    text1: "您已选择" + a.toString(10) + "张图片",
                                                    text2: "点击重新选择"
                                                }) : i.uploadOneByOne(t, e, a);
                                            },
                                            fail: function() {
                                                i.uploadOneByOne(t, e, a);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    });
                } else ++e == a && i.setData({
                    nextable: !0,
                    defaultMsg: "生成",
                    text1: "您已选择" + a.toString(10) + "张图片",
                    text2: "点击重新选择"
                }), i.uploadOneByOne(t, e, a);
            }
        });
    },
    bindTimeChange: function(t) {
        this.setData({
            systime: t.detail.value
        });
    },
    onShareAppMessage: function(t) {
        return t.from, {
            title: "我刚刚用了朋友圈集赞截图生成器，从此集赞不求人，你也试试",
            path: "/pages/index/index",
            imageUrl: "/images/share.jpg",
            success: function(t) {}
        };
    },
    savePhoto: function() {
        wx.saveImageToPhotosAlbum({
            filePath: "../../images/right.jpg",
            success: function(t) {
                console.log("success");
            },
            fail: function(t) {
                console.log(t);
            }
        });
    },
    onHide: function() {
        o.globalData.previousPage = "mode2";
    }
});