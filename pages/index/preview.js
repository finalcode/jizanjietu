require("../../@babel/runtime/helpers/interopRequireDefault")(require("../../components/poster/poster"));

var t = getApp(), e = null;

Page({
    data: {
        height: 0,
        path: ""
    },
    onLoad: function(t) {
        var n = t.path;
        this.setData({
            path: n
        }), wx.createInterstitialAd && ((e = wx.createInterstitialAd({
            adUnitId: "adunit-9bfb099ea4e1f1bb"
        })).onLoad(function() {}), e.onError(function(t) {
            wx.reportAnalytics("interstitialerr", {
                code: t.errCode
            });
        }), e.onClose(function() {})), wx.onUserCaptureScreen(function(t) {
            e && e.show().catch(function(t) {
                console.error(t);
            });
        });
    },
    onReady: function() {
        var t = this;
        wx.getSystemInfo({
            success: function(e) {
                t.setData({
                    height: e.windowHeight
                });
            }
        });
    },
    onShow: function() {
        t.globalData.previousPage = "preview", t.globalData.saveTimes = t.globalData.saveTimes + 1;
    },
    onHide: function() {},
    back: function() {
        wx.navigateBack({
            complete: function(t) {}
        });
    },
    previewFull: function() {
        e && e.show().catch(function(t) {
            console.error(t);
        }), wx.previewImage({
            urls: [ this.data.path ],
            current: this.data.path
        });
    },
    save: function() {
        wx.saveImageToPhotosAlbum({
            filePath: this.data.path,
            success: function(t) {
                wx.showToast({
                    title: "保存成功",
                    icon: "none",
                    duration: 1500
                });
            },
            fail: function(t) {
                wx.showToast({
                    title: "也可以在全屏预览中 长按保存",
                    icon: "none",
                    duration: 1500
                });
            }
        }), e && e.show().catch(function(t) {
            console.error(t);
        });
    },
    onShareAppMessage: function(t) {
        return t.from, {
            title: "我刚刚用了朋友圈集赞截图生成器，从此集赞不求人，你也试试",
            path: "/pages/index/index",
            imageUrl: "/images/share.jpg",
            success: function(t) {}
        };
    }
});