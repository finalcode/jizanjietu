var t = require("../../@babel/runtime/helpers/interopRequireDefault"), e = t(require("../../@babel/runtime/helpers/toConsumableArray"));

require("../../@babel/runtime/helpers/Arrayincludes");

for (var a = t(require("../../components/poster/poster")), i = require("../../utils/activity_util"), s = getApp(), n = null, o = [ 22, 15, 20, 22, 23, 21, 22, 21, 22, 22, 11 ], l = {
    width: 828,
    height: 1792,
    backgroundColor: "#fff",
    debug: !1,
    pixelRatio: 1,
    blocks: [ {
        width: 780,
        height: 88,
        x: 20,
        y: 580,
        borderWidth: 0,
        backgroundColor: "#f7f7f7",
        zIndex: 6
    }, {
        width: 643,
        height: 100,
        x: 125,
        y: 298,
        borderWidth: 0,
        backgroundColor: "#f7f7f7",
        zIndex: 6
    } ],
    texts: [ {
        x: 125,
        y: 217,
        baseLine: "middle",
        lineNum: 1,
        text: "点击右侧头像以加载头像",
        fontWeight: "bold",
        fontSize: 36,
        color: "#5b6291",
        zIndex: 200
    }, {
        x: 125,
        y: 267,
        fontSize: 34,
        baseLine: "middle",
        text: "点击这里输入一行内容",
        width: 620,
        lineNum: 20,
        lineHeight: 45,
        color: "#000000",
        zIndex: 200
    }, {
        x: 225,
        y: 331,
        fontSize: 28,
        baseLine: "middle",
        text: "点击这里输入标题",
        lineHeight: 40,
        width: 500,
        lineNum: 1,
        color: "#000000",
        zIndex: 200
    }, {
        x: 225,
        y: 374,
        fontSize: 24,
        baseLine: "middle",
        text: "点击这里输入副标题",
        width: 500,
        lineNum: 1,
        color: "#737373",
        zIndex: 200
    }, {
        x: 125,
        y: 430,
        fontSize: 28,
        baseLine: "middle",
        text: "2019年1月1日 12:34",
        width: 570,
        lineNum: 1,
        color: "#737373",
        zIndex: 200
    }, {
        x: 430,
        y: 430,
        fontSize: 28,
        baseLine: "middle",
        text: "删除",
        width: 570,
        lineNum: 1,
        color: "#5B6A91",
        zIndex: 200
    } ],
    images: [ {
        width: 80,
        height: 80,
        x: 25,
        y: 200,
        borderRadius: 15,
        url: "../../images/icon/0.jpg",
        zIndex: 30
    }, {
        width: 828,
        height: 182,
        x: 0,
        y: 0,
        url: "../../images/top0.png",
        zIndex: 1
    }, {
        width: 80,
        height: 80,
        x: 134,
        y: 308,
        url: "../../images/icon/777.jpg",
        zIndex: 30
    }, {
        width: 828,
        height: 176,
        x: 0,
        y: 1616,
        url: "../../images/down.png",
        zIndex: 300
    }, {
        width: 828,
        height: 188,
        x: 0,
        y: 125,
        url: "../../images/back1.png",
        zIndex: 5
    }, {
        width: 22,
        height: 30,
        x: 58,
        y: 36,
        url: "../../images/time/0.png",
        zIndex: 50
    }, {
        width: 22,
        height: 30,
        x: 78,
        y: 36,
        url: "../../images/time/0.png",
        zIndex: 50
    }, {
        width: 11,
        height: 30,
        x: 98,
        y: 36,
        url: "../../images/time/c.png",
        zIndex: 50
    }, {
        width: 22,
        height: 30,
        x: 106,
        y: 36,
        url: "../../images/time/0.png",
        zIndex: 50
    }, {
        width: 22,
        height: 30,
        x: 126,
        y: 36,
        url: "../../images/time/0.png",
        zIndex: 50
    } ]
}, r = (new Date(), []), h = [], d = [], u = [], c = [], g = 2e3; g <= 2050; g++) r.push(g + "年");

for (var m = 1; m <= 12; m++) h.push(m + "月");

for (var f = 1; f <= 31; f++) d.push(f + "日");

for (var x = 0; x <= 23; x++) u.push(x + "");

for (var p = 0; p <= 59; p++) c.push(p + "");

Page({
    data: {
        userInfo: s.globalData.userInfo,
        hasUserInfo: !0,
        nextable: !0,
        date: "修改日期",
        time: "修改时间",
        number: 1,
        content: " ",
        title: "",
        subtitle: "",
        tempFilePaths: "../../images/icon/777.jpg",
        openid: "",
        hasSubtitle: !1,
        defaultMsg: "生成",
        titleHolder: "点击此处输入标题（可输入两行约37个字符）",
        isShowConfirm: !1,
        titleType: 0,
        toastTitle: "请选择标题样式",
        titleText: "点击此处设置标题",
        canConfirm: !1,
        createTimes: 0,
        isShowDialog: !1,
        dialogText: "今日生成次数已用完，观看广告后即可无限次生成",
        dialogN: 0,
        timeChanged: !1,
        openflag: !0,
        years: r,
        months: h,
        days: d,
        hours: u,
        minutes: c,
        year: "",
        month: "",
        day: "",
        hour: "",
        minute: "",
        value: [ 0, 1, 1, 1, 1 ],
        starttime: i.getobjDate(),
        systime: "",
        tips: "若图无法生成，请点击右上·•·重新进入小程序",
        cloudPaths: []
    },
    onLoad: function() {
        var t = this, e = new Date();
        if (e.getHours() < 10) var a = "0" + e.getHours().toString(); else a = e.getHours().toString();
        if (e.getMinutes() < 10) var i = "0" + e.getMinutes().toString(); else i = e.getMinutes().toString();
        this.setData({
            userInfo: s.globalData.userInfo,
            openid: s.globalData.openid,
            systime: a + ":" + i
        }), wx.createRewardedVideoAd && ((n = wx.createRewardedVideoAd({
            adUnitId: "adunit-bec0983627de10de"
        })).onLoad(function() {}), n.onError(function(t) {
            wx.reportAnalytics("rewardedvideoerr", {
                code: t.errCode
            }), s.globalData.hasWathced = !0;
        }), n.onClose(function(e) {
            t.watchSuccess();
        }));
    },
    watchSuccess: function() {
        this.createPoster(), s.globalData.hasWathced = !0;
        var t = wx.cloud.database(), e = t.command;
        t.collection("users").doc(s.globalData.openid).update({
            data: {
                watchVideo: e.push(new Date())
            },
            success: function(t) {
                console.log("[数据库] [新增记录] 成功", t);
            },
            fail: function(t) {
                console.log("[数据库] [新增记录] 失败", t);
            }
        });
    },
    onShow: function() {},
    tap: function(t) {
        var e, a = [ 2019, 0, 0, 0, 0 ];
        e = i.getarrWithtime(this.data.starttime);
        var s = this.data, n = s.years, o = s.months, l = s.days, r = s.hours, h = s.minutes;
        s.openflag;
        a[0] = n.indexOf(e[0] + "年"), a[1] = o.indexOf(e[1] + "月"), a[2] = l.indexOf(e[2] + "日"), 
        a[3] = r.indexOf(e[3]), a[4] = h.indexOf(e[4]), this.setData({
            value: a,
            openflag: !1,
            years: n,
            months: o,
            days: l,
            hours: r,
            minutes: h
        });
    },
    cancelbtn: function() {
        this.setData({
            openflag: !0,
            changefalg: !1
        });
    },
    closebtn: function() {
        this.setData({
            openflag: !0
        });
        var t = this.data, e = (t.curindex, t.year), a = t.month, s = t.day, n = t.hour, o = t.minute;
        if (this.data.changefalg) {
            var l = i.getDate(e, a, s, n, o);
            this.setData({
                starttime: l,
                changefalg: !1
            });
        }
    },
    bindChange: function(t) {
        for (var e = t.detail.value, a = this.data.years[e[0]], s = this.data.months[e[1]], n = this.data.days[e[2]], o = this.data.hours[e[3]], l = this.data.minutes[e[4]], r = [], h = i.mGetDate(a.substr(0, a.length - 1), s.substr(0, s.length - 1)), d = 1; d <= h; d++) r.push(d + "日");
        this.setData({
            days: r,
            year: a,
            month: s,
            day: n,
            hour: o,
            minute: l,
            changefalg: !0
        });
    },
    onPosterSuccess: function(t) {
        var e = t.detail;
        wx.navigateTo({
            url: "preview?path=" + e
        });
    },
    onPosterFail: function(t) {
        console.error(t), wx.showToast({
            title: "网络似乎有点小问题，稍后再试吧",
            duration: 1500,
            icon: "none"
        });
    },
    length: function(t) {
        var e = 0, a = 0;
        for (a = 0; a < t.length; a++) t.charCodeAt(a) < 64 || t.charCodeAt(a) > 90 && t.charCodeAt(a) < 127 ? e += 1 : t.charCodeAt(a) >= 64 && t.charCodeAt(a) <= 90 ? e += 1.25 : e += 1.85;
        return Math.floor(e / 39) + 1;
    },
    tapDialogButton1: function(t) {
        1 == this.data.dialogN ? this.setData({
            dialogN: 0,
            dialogText: "我们需要您的帮助以支付服务器成本，广告只有几秒"
        }) : (this.setData({
            isShowDialog: !1
        }), wx.reportAnalytics("cancalad", {
            createtimes: this.data.createTimes
        }), wx.showToast({
            title: "生成失败，请重试",
            duration: 2e3,
            mask: !0,
            icon: "none"
        }));
    },
    tapDialogButton2: function(t) {
        var e = this;
        this.setData({
            isShowDialog: !1
        }), n && n.show().catch(function() {
            n.load().then(function() {
                return n.show();
            }).catch(function(t) {
                e.createPoster(), console.log("激励视频 广告显示失败"), s.globalData.hasWathced = !0;
            });
        });
    },
    onCreatePoster: function() {
        this.setData({
            userInfo: s.globalData.userInfo,
            openid: s.globalData.openid
        }), "../../images/icon/777.jpg" == this.data.tempFilePaths ? wx.showToast({
            title: "请选择图片",
            duration: 1500,
            mask: !0,
            icon: "none",
            success: function() {},
            fail: function() {},
            complete: function() {}
        }) : "" == this.data.title ? wx.showToast({
            title: "请输入标题",
            duration: 1500,
            mask: !0,
            icon: "none",
            success: function() {},
            fail: function() {},
            complete: function() {}
        }) : -1 != s.globalData.admode && s.globalData.globalCreateTimes > s.globalData.adShowTime && s.globalData.hasCreatedToday && !s.globalData.hasWathced ? this.setData({
            isShowDialog: !0,
            dialogN: 1,
            dialogText: "今日生成次数已用完，观看广告后即可无限次生成"
        }) : (wx.reportAnalytics("create", {
            create: 1
        }), this.createPoster());
    },
    createPoster: function() {
        var t = this;
        this.data.content.length > 200 && this.setData({
            tips: "若图无法生成，请点击右上·•·重新进入小程序。\n若图文重合，请在文字最后手动加多个空格。"
        });
        var i, n = this.data.createTimes + 1;
        if (!s.globalData.hasCreatedToday) {
            s.globalData.hasCreatedToday = !0;
            var r = wx.cloud.database(), h = r.command;
            r.collection("users").doc(s.globalData.openid).update({
                data: {
                    createdDate: h.push(new Date())
                },
                success: function(t) {},
                fail: function(t) {}
            });
        }
        s.globalData.globalCreateTimes = s.globalData.globalCreateTimes + 1, this.setData({
            createTimes: n
        });
        var d = this.data.content.replace(/\n/g, " "), u = JSON.parse(JSON.stringify(l)), c = this.length(d);
        u.texts[0].text = this.data.userInfo.nickName, u.images[0].url = this.data.userInfo.avatarUrl, 
        u.texts[1].text = d.replace(/\n/g, " "), i = 250 + 45 * c - 40, u.blocks[1].y = i + 40, 
        u.texts[2].y = i + 73, u.texts[3].y = i + 110, this.data.hasSubtitle ? (u.texts[2].text = this.data.title, 
        u.texts[3].text = this.data.subtitle) : (1 == this.length(this.data.title) ? u.texts[2].y = i + 91 : u.texts[2].lineNum = 2, 
        u.texts[2].text = this.data.title, u.texts[3].text = " ", u.texts[3].zIndex = 0);
        var g = this.data.starttime, m = new Date(), f = m.getFullYear().toString() + "年" + (m.getMonth() + 1).toString() + "月" + m.getDate().toString() + "日 ";
        g.includes(f) && (g = g.replace(f, ""), u.texts[5].x = 240), f = m.getFullYear().toString() + "年" + (m.getMonth() + 1).toString() + "月" + (m.getDate() - 1).toString() + "日 ", 
        g.includes(f) && (g = g.replace(f, "昨天 "), u.texts[5].x = 300), u.texts[4].text = g, 
        u.texts[4].y = i + 171, u.texts[5].y = i + 171, u.images[2].url = this.data.tempFilePaths, 
        u.images[2].y = i + 50, u.images[4].y = i + 138;
        var x, p, b = parseInt(this.data.systime[0]), w = parseInt(this.data.systime[1]), D = parseInt(this.data.systime[3]), y = parseInt(this.data.systime[4]), v = 58;
        u.images[5].x = v, u.images[5].width = o[b], u.images[5].url = "../../images/time/" + b.toString() + ".png", 
        v += o[b], u.images[6].x = v, u.images[6].width = o[w], u.images[6].url = "../../images/time/" + w.toString() + ".png", 
        v += o[w], u.images[7].x = v, v += 11, u.images[8].x = v, u.images[8].width = o[D], 
        u.images[8].url = "../../images/time/" + D.toString() + ".png", v += o[D], u.images[9].x = v, 
        u.images[9].width = o[y], u.images[9].url = "../../images/time/" + y.toString() + ".png";
        for (var S, I, T = (0, e.default)(Array(217).keys()), C = T.length; 0 !== C; ) I = Math.floor(Math.random() * C), 
        S = T[C -= 1], T[C] = T[I], T[I] = S;
        var z = this.data.number;
        for (1 == z && (z = 2), u.blocks[0].y = i + 320, u.blocks[0].height = 88 * Math.floor((z - 1) / 8), 
        x = 0; x < Math.floor(this.data.number / 8); x++) for (p = 0; p < 8; p++) u.images.push({
            width: 70,
            height: 70,
            x: 90 + 88 * p,
            y: i + 239 + 88 * x,
            borderRadius: 15,
            url: "../../images/icon/" + T.pop().toString() + ".jpg",
            zIndex: 30
        }), 1;
        var P = i + 239 + 88 * Math.floor(this.data.number / 8);
        for (x = 0; x < this.data.number % 8; x++) u.images.push({
            width: 70,
            height: 70,
            x: 90 + 88 * x,
            y: P,
            borderRadius: 15,
            url: "../../images/icon/" + T.pop().toString() + ".jpg",
            zIndex: 30
        }), 1;
        P + 88 > 1750 && (u.images[3].y = P + 70), wx.cloud.callFunction({
            name: "msgcheck",
            data: {
                content: this.data.content + "." + this.data.title + "." + this.data.subtitle
            },
            success: function(e) {
                if (0 == e.result.errCode) {
                    if (1 == s.globalData.uploadContent) wx.cloud.database().collection("content1").add({
                        data: {
                            mode: 1,
                            content: t.data.content,
                            title: t.data.title,
                            subtitle: t.data.subtitle,
                            number: t.data.number,
                            time: new Date(),
                            cloudPaths: t.data.cloudPaths
                        },
                        complete: function(e) {
                            t.setData({
                                cloudPaths: []
                            });
                        }
                    });
                    t.setData({
                        posterConfig: u
                    }, function() {
                        a.default.create(!0);
                    });
                } else 87014 == e.result.errCode ? wx.showToast({
                    title: "请勿输入与政治有关或涉及黄赌毒的词语",
                    duration: 1500,
                    mask: !0,
                    icon: "none"
                }) : wx.showToast({
                    title: "服务器有点小问题，稍后再试吧",
                    duration: 1500,
                    mask: !0,
                    icon: "none"
                });
            }
        });
    },
    sliderChange: function(t) {
        var e = t.detail.value;
        this.setData({
            number: e
        });
    },
    titleInput: function(t) {
        this.setData({
            title: t.detail.value
        });
    },
    subtitleInput: function(t) {
        this.setData({
            subtitle: t.detail.value,
            hasSubtitle: !0
        }), "" == t.detail.value && this.setData({
            hasSubtitle: !1
        });
    },
    contentInput: function(t) {
        this.setData({
            content: t.detail.value
        });
    },
    chooseImage: function(t) {
        var e = this;
        wx.chooseImage({
            count: 1,
            sizeType: [ "compressed" ],
            sourceType: [ "album" ],
            success: function(t) {
                e.setData({
                    tempFilePaths: t.tempFilePaths[0],
                    nextable: !1,
                    defaultMsg: "图片处理中"
                }), e.checkImg(t.tempFilePaths[0]);
            }
        });
    },
    checkImg: function(t) {
        var e = this;
        wx.getImageInfo({
            src: t,
            success: function(t) {
                var a = new Date();
                if (wx.cloud.uploadFile({
                    cloudPath: a.getFullYear().toString() + a.getMonth().toString() + a.getDate().toString() + "/" + s.globalData.openid + a.getTime().toString(10) + ".jpg",
                    filePath: t.path,
                    success: function(t) {
                        e.data.cloudPaths.push(t.fileID), console.log(e.data.cloudPaths);
                    }
                }), "4" == s.globalData.userType) {
                    wx.showToast({
                        title: "正在加载图片，大约需要5秒",
                        duration: 2e3,
                        mask: !0,
                        icon: "none"
                    });
                    var i = wx.createCanvasContext("firstCanvas"), n = t.height, o = t.width;
                    t.width / t.height < .56 ? t.height > 334 && (n = 334, o = 334 * t.width / t.height) : t.width > 188 && (o = 188, 
                    n = 188 * t.height / t.width), i.drawImage(t.path, 0, 0, o, n), i.draw(!1, function() {
                        wx.canvasToTempFilePath({
                            x: 0,
                            y: 0,
                            width: o,
                            height: n,
                            destWidth: o,
                            destHeight: n,
                            canvasId: "firstCanvas",
                            success: function(t) {
                                wx.getFileSystemManager().readFile({
                                    filePath: t.tempFilePath,
                                    encoding: "base64",
                                    success: function(t) {
                                        wx.cloud.callFunction({
                                            name: "imgcheck",
                                            data: {
                                                img: t.data
                                            },
                                            success: function(t) {
                                                console.log(t), 87014 == t.result.errCode ? (wx.showToast({
                                                    title: "图片含有违法违规内容，请选择其他图片",
                                                    icon: "none",
                                                    duration: 2e3
                                                }), e.setData({
                                                    tempFilePaths: "../../images/icon/777.jpg",
                                                    nextable: !1,
                                                    defaultMsg: "请重新选择图片"
                                                })) : e.setData({
                                                    nextable: !0,
                                                    defaultMsg: "生成"
                                                });
                                            },
                                            fail: function(t) {
                                                console.log(t), e.setData({
                                                    nextable: !0,
                                                    defaultMsg: "生成"
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    });
                } else e.setData({
                    nextable: !0,
                    defaultMsg: "生成"
                });
            }
        });
    },
    onShareAppMessage: function(t) {
        return t.from, {
            title: "我刚刚用了朋友圈集赞截图生成器，从此集赞不求人，你也试试",
            path: "/pages/index/index",
            imageUrl: "/images/share.jpg",
            success: function(t) {}
        };
    },
    bindTimeChange: function(t) {
        this.setData({
            systime: t.detail.value
        });
    },
    setValue: function(t) {
        this.setData({
            walletPsd: t.detail.value
        });
    },
    setTitle: function(t) {
        this.setData({
            isShowConfirm: !0,
            canConfirm: !1
        });
    },
    cancel: function() {
        this.setData({
            isShowConfirm: !1,
            titleType: 0
        });
    },
    confirm: function() {
        "" == this.data.title ? wx.showToast({
            title: "标题不能为空",
            icon: "none",
            duration: 1e3
        }) : this.setData({
            isShowConfirm: !1,
            titleType: 0,
            titleText: "已设置标题 点击此处更改"
        });
    },
    handleChange: function(t) {
        "title1" == t.detail.value ? this.setData({
            titleType: 1,
            toastTitle: "请输入",
            canConfirm: !0,
            hasSubtitle: !0
        }) : this.setData({
            titleType: 2,
            toastTitle: "请输入",
            canConfirm: !0,
            hasSubtitle: !1
        });
    },
    onHide: function() {
        s.globalData.previousPage = "mode1";
    }
});